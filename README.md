			      FlashCards

	      A LaTeX Class for Typesetting Flash Cards.

				v1.0.1
			     30 July 2010

		 Alexander M. Budge <ambudge@mit.edu>
(bug-fixes from v1.0.0 to v1.0.1 by Matthew Vernon <matthew@debian.org>)

----------------------------------------------------------------------
			     DESCRIPTION

The FlashCards  class provides for  the typesetting of flash  cards in
LaTeX2e.  By flash card, I mean a two sided card which has a prompt or
a question  on one  side and the  response or  the answer on  the flip
(back) side.  Flash  cards come in many sizes  depending on the nature
of  the information  they contain.  In particular,  I like  using both
3x5'' (index  cards) and 2x3.5'' (business cards)  which are available
in perforated sheets suitable for printing or copying onto.


----------------------------------------------------------------------
		       CONTENTS OF DISTRIBUTION

The package files:
  README			This file
  COPYING			The GNU Public License
  flashcards.dtx		Documentation and source code
  flahcards.ins			Installation driver

These files generate:
  flashcards.cls		FlashCards class source code
  avery5371.cfg			Avery business cards configuration
  avery5388.cfg			Avery index cards configuration
  samplecards.tex		Sample document


----------------------------------------------------------------------
			     INSTALLATION

To generate the documentation, run LaTeX on the file "flashcards.dtx".

To generate the class file, sample document and configuration files,
run LaTeX on the file "flashcards.ins".  Place the class file (ending
with '.cls') and the configuration files (ending with '.cfg') in your
LaTeX path.


----------------------------------------------------------------------
                       DISTRIBUTION CONDITIONS

     FlashCards LaTeX2e Class for Typesetting Double Sided Cards
       Copyright (C) 2000  Alexander M. Budge <ambudge@mit.edu>

This program is  free software; you can redistribute  it and/or modify
it under the  terms of the GNU General Public  License as published by
the Free Software Foundation; either  version 2 of the License, or (at
your option) any later version.

This program  is distributed in the  hope that it will  be useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with this program (the file  COPYING); if not, write to the Free
Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.



